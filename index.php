<?php
    require_once("animal.php");
    require_once("frog.php");
    require_once("ape.php");
    $sheep = new animal("Shaun");

    echo "Nama : $sheep->name <br>"; // "shaun"
    echo "Jumlah Kaki : $sheep->legs <br>"; // 2
    echo "Darah : $sheep->cold_blooded <br><br>"; // false

    $kodok = new frog("Buduk");

    echo "Nama : $kodok->name <br>";
    echo "Jumlah Kaki : $kodok->legs <br>";
    echo "Darah : $kodok->cold_blooded <br>";
    $kodok->jump() ; // "hop hop"

    $sungokong = new ape("Kera Sakti");
    echo "Nama : $sungokong->name <br>";
    echo "Jumlah Kaki : $sungokong->legs <br>";
    echo "Darah : $sungokong->cold_blooded <br>";
    $sungokong->yell(); // "Auooo"

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())
?>